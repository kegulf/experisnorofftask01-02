using System;

namespace Introday {

    class IntroDay {

        static void Main(string[] args) {
            Console.WriteLine("\tHello! \n\tWhich task would you like to run?" +
                "\n\t---------------------------------" +
                "\n\t| 0. Task 0 - Hello World\t|" +
                "\n\t| 1. Task 1 - Hello lecturers\t|" +
                "\n\t| 2. Task 2 - Basic input\t|" +
                "\n\t| 3. Exit program\t\t|" +
                "\n\t---------------------------------");

            int usersChoiceOfTask = Convert.ToInt32(Console.ReadLine());

            switch (usersChoiceOfTask) {
                case 0:
                    HelloWorld();
                    break;
                case 1:
                    HelloLecturers();
                    break;
                case 2:
                    BasicInput();
                    break;
                case 3:
                    ExitProgram();
                    break;
                default:
                    Console.WriteLine("\nPlease choose a valid input (0-3)");
                    Main(null);
                    break;
            }

            // Ask if the user wants to go again.
            string usersChoiceToRetry = "";
            while (!(usersChoiceToRetry.Equals("y") || usersChoiceToRetry.Equals("n"))) {
                Console.WriteLine("\n\nDo you want to go again? (y/n)");
                usersChoiceToRetry = Console.ReadLine();
            }

            // If yes, rerun Main
            if (usersChoiceToRetry.Equals("y")) {
                Main(null);
            }
            else {
                ExitProgram();
            }
        }

        /// <summary>
        /// Basic HelloWorld method
        /// </summary>
        static void HelloWorld() {
            Console.WriteLine("\nHello World!");
        }

        /// <summary>
        /// Task 1, printing a string that isn't "Hello World" :O
        /// </summary>
        static void HelloLecturers() {
            Console.WriteLine("\nHello Dean and Greg.\nMy name is Odd, and I'm quite odd. *Badum tshh*");
        }

        /// <summary>
        /// Task 2, testing out basic input.
        /// </summary>
        static void BasicInput() {
            Console.WriteLine("\nHello there, what's your name?");

            // Get name and define some details about it.
            string fullName = Console.ReadLine();
            string spacelessName = fullName.Replace(" ", "");
            string initials = GetInitials(fullName);

            // Say Hello
            Console.WriteLine("\nHello " + fullName);
            Console.WriteLine("Your name is " + spacelessName.Length + " characters long.");

            // If there is spaces in the name, print the length of the name, counting the spaces.
            if (fullName.Contains(" ")) {
                Console.WriteLine("If you count the spaces it's " + fullName.Length + " characters long");
            }

            Console.WriteLine("The first letter of your name is " + fullName.Substring(0, 1));

            // If the user has more than one name, print his/her initials.
            if(initials.Length > 1) { 
                Console.WriteLine("Your initials are: " + initials);
            }
        }

      
        /// <summary>
        /// Takes a string representing a name, splits it on space and returns 
        /// a string containing the first letter of each name.
        /// </summary>
        /// <param name="fullName">The name the user inputed</param>
        /// <returns>A string containing the first letter of each name</returns>
        static string GetInitials(string fullName) {

            string[] names = fullName.Split(" ");
            string initials = "";

            foreach(string name in names) {
                initials += name.Substring(0, 1).ToUpper();
            }

            return initials;
        }

        /// <summary>
        /// Exits the program with status 0 (everything OK)
        /// </summary>
        static void ExitProgram() {
            Console.WriteLine("\nOkay then! \nThank you for choosing an Odd service! \nBye now");
            Environment.Exit(0);
        }
    }
}
